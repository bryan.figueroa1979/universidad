from django.shortcuts import render, redirect
from .models import CarreraBAFA
from .models import CursoBAFA
from .models import AsignaturaBAFA
from django.contrib import messages

# Create your views here.
def listadoCarreras(request):
    carrerasBdd = CarreraBAFA.objects.all()
    return render(request, 'listadoCarreras.html', {'carreras': carrerasBdd})

def guardarCarrera(request):
    if request.method == 'POST':
        nombreCarreraBAFA = request.POST['nombreCarreraBAFA']
        directorCarreraBAFA = request.POST['directorCarreraBAFA']
        duracionCarreraBAFA = request.POST['duracionCarreraBAFA']
        requisitosIngresoBAFA = request.POST['requisitosIngresoBAFA']


        logoCarreraBAFA = request.FILES.get('logoCarreraBAFA')
        nuevaCarrera = CarreraBAFA.objects.create(
            nombreCarreraBAFA=nombreCarreraBAFA,
            directorCarreraBAFA=directorCarreraBAFA,
            duracionCarreraBAFA=duracionCarreraBAFA,
            requisitosIngresoBAFA=requisitosIngresoBAFA,
            logoCarreraBAFA=logoCarreraBAFA

        )
        messages.success(request, 'Carrera Agregada Exitosamente')
        return redirect('/')  # Ajusta el nombre de la URL según tu configuración

    return render(request, 'guardarCarrera.html')  # Ajusta el nombre del template según tu configuración

def eliminarCarrera(request, idCarreraBAFA):
    carreraEliminar = CarreraBAFA.objects.get(idCarreraBAFA=idCarreraBAFA)
    carreraEliminar.delete()
    messages.error(request, 'Carrera Eliminada Exitosamente')
    return redirect('/')  # Ajusta el nombre de la URL según tu configuración

def editarCarrera(request,idCarreraBAFA):
    carreraEditar=CarreraBAFA.objects.get(idCarreraBAFA=idCarreraBAFA)
    return render(request,'editarCarrera.html',{'carrera':carreraEditar})

def procesarActualizacionCarrera(request):
    idCarreraBAFA=request.POST["idCarreraBAFA"]
    nombreCarreraBAFA = request.POST['nombreCarreraBAFA']
    directorCarreraBAFA = request.POST['directorCarreraBAFA']
    duracionCarreraBAFA = request.POST['duracionCarreraBAFA']
    requisitosIngresoBAFA = request.POST['requisitosIngresoBAFA']
    logoCarreraBAFA = request.FILES.get('logoCarreraBAFA')
    #Insertando datos mediante el ORM de DJANGO
    carreraEditar=CarreraBAFA.objects.get(idCarreraBAFA=idCarreraBAFA)
    carreraEditar.nombreCarreraBAFA=nombreCarreraBAFA
    carreraEditar.directorCarreraBAFA=directorCarreraBAFA
    carreraEditar.duracionCarreraBAFA=duracionCarreraBAFA
    carreraEditar.requisitosIngresoBAFA=requisitosIngresoBAFA
    # Verificar si se proporcionó una nueva imagen
    if logoCarreraBAFA:
        carreraEditar.logoCarreraBAFA = logoCarreraBAFA
    carreraEditar.save()
    messages.warning(request,
      'Carrera Actualizada Correctamente')
    return redirect('/')

#Cursos

def listadoCursos(request):
    cursosBdd = CursoBAFA.objects.all()
    carrerasBdd=CarreraBAFA.objects.all()
    return render(request, 'listadoCursos.html', {'cursos': cursosBdd, 'carreras': carrerasBdd})

def guardarCurso(request):
    if request.method == 'POST':
        nivelCursoBAFA = request.POST['nivelCursoBAFA']
        descripcionCursoBAFA = request.POST['descripcionCursoBAFA']
        aulaCursoBAFA = request.POST['aulaCursoBAFA']
        fechaInicioBAFA = request.POST['fechaInicioBAFA']
        profesorPrincipalBAFA = request.POST['profesorPrincipalBAFA']
        carrera_id = request.POST['carrera_id']  # Asegúrate de tener el campo 'carrera' en tu formulario
        carreraSelecionada=CarreraBAFA.objects.get(idCarreraBAFA=carrera_id)

        nuevoCurso = CursoBAFA.objects.create(
            nivelCursoBAFA=nivelCursoBAFA,
            descripcionCursoBAFA=descripcionCursoBAFA,
            aulaCursoBAFA=aulaCursoBAFA,
            fechaInicioBAFA=fechaInicioBAFA,
            profesorPrincipalBAFA=profesorPrincipalBAFA,
            carrera=carreraSelecionada
        )
        messages.success(request, 'Curso Agregado Exitosamente')
        return redirect('/listadoCursos/')  # Ajusta el nombre de la URL según tu configuración

def editarCurso(request,idCursoBAFA):
    cursoEditar=CursoBAFA.objects.get(idCursoBAFA=idCursoBAFA)
    carrerasBdd=CarreraBAFA.objects.all()
    return render(request,'editarCurso.html',{'curso':cursoEditar,'carreras':carrerasBdd})

def eliminarCurso(request, idCursoBAFA):
    cursoEliminar = CursoBAFA.objects.get(idCursoBAFA=idCursoBAFA)
    cursoEliminar.delete()
    messages.error(request, 'Curso Eliminado Exitosamente')
    return redirect('/listadoCursos/')  # Ajusta el nombre de la URL según tu configuración

def procesarActualizacionCurso(request):
    idCursoBAFA=request.POST["idCursoBAFA"]
    nivelCursoBAFA = request.POST['nivelCursoBAFA']
    descripcionCursoBAFA = request.POST['descripcionCursoBAFA']
    aulaCursoBAFA = request.POST['aulaCursoBAFA']
    fechaInicioBAFA = request.POST['fechaInicioBAFA']
    profesorPrincipalBAFA = request.POST['profesorPrincipalBAFA']
    carrera_id = request.POST['carrera_id']  # Asegúrate de tener el campo 'carrera' en tu formulario
    carreraSelecionada=CarreraBAFA.objects.get(idCarreraBAFA=carrera_id)
    #Insertando datos mediante el ORM de DJANGO
    cursoEditar=CursoBAFA.objects.get(idCursoBAFA=idCursoBAFA)
    cursoEditar.carrera=carreraSelecionada
    cursoEditar.nivelCursoBAFA=nivelCursoBAFA
    cursoEditar.descripcionCursoBAFA=descripcionCursoBAFA
    cursoEditar.aulaCursoBAFA=aulaCursoBAFA
    cursoEditar.fechaInicioBAFA=fechaInicioBAFA
    cursoEditar.profesorPrincipalBAFA=profesorPrincipalBAFA
    cursoEditar.save()
    messages.warning(request,
      'Curso Actualizado Exitosamente')
    return redirect('/listadoCursos/')

#Asignaturas

def listadoAsignaturas(request):
    asignaturasBdd=AsignaturaBAFA.objects.all()
    cursosBdd=CursoBAFA.objects.all()
    return render(request, 'listadoAsignaturas.html',{'asignaturas':asignaturasBdd,'cursos':cursosBdd})

def guardarAsignatura(request):
    #campurando los valores del formulario por POST
    id_curso=request.POST["id_curso"]
    #campurando el tipo seleccionado por el usuario
    cursoSeleccionado=CursoBAFA.objects.get(idCursoBAFA=id_curso)
    nombreAsignaturaBAFA=request.POST["nombreAsignaturaBAFA"]
    creditosAsignaturaBAFA=request.POST["creditosAsignaturaBAFA"]
    fechaInicioAsignaturaBAFA=request.POST["fechaInicioAsignaturaBAFA"]
    fechaFinalizacionAsignaturaBAFA=request.POST["fechaFinalizacionAsignaturaBAFA"]
    profesorAsignaturaBAFA=request.POST["profesorAsignaturaBAFA"]
    metodoEvaluacionBAFA=request.POST["metodoEvaluacionBAFA"]
    recursosDidacticosBAFA=request.POST["recursosDidacticosBAFA"]
    #capturar el archivos que viene de listadoClientes
    silaboAsignaturaBAFA=request.FILES.get("silaboAsignaturaBAFA")#("fotografia"): viene del name que usa fotografia en el listadoClientes
    #Insertando datos mediante el ORM de DJango
    nuevoAsignatura=AsignaturaBAFA.objects.create(
        curso=cursoSeleccionado,
        nombreAsignaturaBAFA=nombreAsignaturaBAFA,
        creditosAsignaturaBAFA=creditosAsignaturaBAFA,
        fechaInicioAsignaturaBAFA=fechaInicioAsignaturaBAFA,
        fechaFinalizacionAsignaturaBAFA=fechaFinalizacionAsignaturaBAFA,
        profesorAsignaturaBAFA=profesorAsignaturaBAFA,
        metodoEvaluacionBAFA=metodoEvaluacionBAFA,
        recursosDidacticosBAFA=recursosDidacticosBAFA,
        silaboAsignaturaBAFA=silaboAsignaturaBAFA
    )
    messages.success(request,'Asignatura Guradada Exitosamente')
    return redirect('/listadoAsignaturas/')

def eliminarAsigantura(request,idAsignaturaBAFA):
    asignaturaEliminar=AsignaturaBAFA.objects.get(idAsignaturaBAFA=idAsignaturaBAFA)
    asignaturaEliminar.delete()
    messages.error(request,'Asignatura Eliminada Exitosamente')
    return redirect('/listadoAsignaturas')

def editarAsignatura(request,idAsignaturaBAFA):
    AsignaturaEditar=AsignaturaBAFA.objects.get(idAsignaturaBAFA=idAsignaturaBAFA)
    cursosBdd=CursoBAFA.objects.all()
    return render(request,'editarAsignatura.html',{'asignatura':AsignaturaEditar,'cursos':cursosBdd})

def procesarActualizacionAsignatura(request):
    idAsignaturaBAFA=request.POST["idAsignaturaBAFA"]
    id_curso=request.POST["id_curso"]
    #campurando el tipo seleccionado por el usuario
    cursoSeleccionado=CursoBAFA.objects.get(idCursoBAFA=id_curso)
    nombreAsignaturaBAFA=request.POST["nombreAsignaturaBAFA"]
    creditosAsignaturaBAFA=request.POST["creditosAsignaturaBAFA"]
    fechaInicioAsignaturaBAFA=request.POST["fechaInicioAsignaturaBAFA"]
    fechaFinalizacionAsignaturaBAFA=request.POST["fechaFinalizacionAsignaturaBAFA"]
    profesorAsignaturaBAFA=request.POST["profesorAsignaturaBAFA"]
    metodoEvaluacionBAFA=request.POST["metodoEvaluacionBAFA"]
    recursosDidacticosBAFA=request.POST["recursosDidacticosBAFA"]
    #capturar el archivos que viene de listadoClientes
    silaboAsignaturaBAFA=request.FILES.get("silaboAsignaturaBAFA")
    #Insertando datos mediante el ORM de DJANGO
    AsignaturaEditar=AsignaturaBAFA.objects.get(idAsignaturaBAFA=idAsignaturaBAFA)
    AsignaturaEditar.curso=cursoSeleccionado
    AsignaturaEditar.nombreAsignaturaBAFA=nombreAsignaturaBAFA
    AsignaturaEditar.creditosAsignaturaBAFA=creditosAsignaturaBAFA
    AsignaturaEditar.fechaInicioAsignaturaBAFA=fechaInicioAsignaturaBAFA
    AsignaturaEditar.fechaFinalizacionAsignaturaBAFA=fechaFinalizacionAsignaturaBAFA
    AsignaturaEditar.profesorAsignaturaBAFA=profesorAsignaturaBAFA
    AsignaturaEditar.metodoEvaluacionBAFA=metodoEvaluacionBAFA
    AsignaturaEditar.recursosDidacticosBAFA=recursosDidacticosBAFA
    # Verificar si se proporcionó una nueva imagen
    if silaboAsignaturaBAFA:
        AsignaturaEditar.silaboAsignaturaBAFA = silaboAsignaturaBAFA
    AsignaturaEditar.save()
    messages.warning(request,
      'Asignatura Acuralizado Exitosamente')
    return redirect('/listadoAsignaturas/')
