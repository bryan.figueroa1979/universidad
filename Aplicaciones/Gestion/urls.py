from django.urls import path
from . import views

urlpatterns=[
    path('',views.listadoCarreras),
    path('guardarCarrera/',views.guardarCarrera),
    path('eliminarCarrera/<idCarreraBAFA>',views.eliminarCarrera),
    path('editarCarrera/<idCarreraBAFA>',views.editarCarrera),
    path('procesarActualizacionCarrera/',views.procesarActualizacionCarrera),

    path('listadoCursos/',views.listadoCursos, name='curso'),
    path('guardarCurso/',views.guardarCurso),
    path('eliminarCurso/<idCursoBAFA>',views.eliminarCurso),
    path('editarCurso/<idCursoBAFA>',views.editarCurso),
    path('procesarActualizacionCurso/',views.procesarActualizacionCurso),

    path('listadoAsignaturas/',views.listadoAsignaturas, name='asignatura'),
    path('guardarAsignatura/',views.guardarAsignatura),
    path('eliminarAsigantura/<idAsignaturaBAFA>',views.eliminarAsigantura),
    path('editarAsignatura/<idAsignaturaBAFA>',views.editarAsignatura),
    path('procesarActualizacionAsignatura/',views.procesarActualizacionAsignatura),
]
