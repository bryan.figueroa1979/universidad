from django.db import models

# Create your models here.

class CarreraBAFA(models.Model):
    idCarreraBAFA = models.AutoField(primary_key=True)
    nombreCarreraBAFA = models.CharField(max_length=255)
    directorCarreraBAFA = models.CharField(max_length=255)
    logoCarreraBAFA = models.FileField(upload_to='carreras', null=True, blank=True)  # Ajusta el directorio de carga según tus preferencias
    duracionCarreraBAFA = models.IntegerField()  # Duración de la carrera en años
    requisitosIngresoBAFA = models.TextField()

    def __str__(self):
        fila="{0}: {1} {2} {3} {4} {5}"
        return fila.format(self.idCarreraBAFA, self.nombreCarreraBAFA, self.directorCarreraBAFA, self.logoCarreraBAFA, self.duracionCarreraBAFA, self.requisitosIngresoBAFA)

class CursoBAFA(models.Model):
    idCursoBAFA = models.AutoField(primary_key=True)
    nivelCursoBAFA = models.CharField(max_length=255)
    descripcionCursoBAFA = models.TextField()
    aulaCursoBAFA = models.CharField(max_length=50)
    fechaInicioBAFA = models.DateField()
    profesorPrincipalBAFA = models.CharField(max_length=255)
    carrera=models.ForeignKey(CarreraBAFA, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        fila = "{0}: {1} {2} {3} {4} {5}"
        return fila.format(
            self.idCursoBAFA, self.nivelCursoBAFA, self.descripcionCursoBAFA,
            self.aulaCursoBAFA, self.fechaInicioBAFA, self.profesorPrincipalBAFA
        )

class AsignaturaBAFA(models.Model):
    idAsignaturaBAFA = models.AutoField(primary_key=True)
    nombreAsignaturaBAFA = models.CharField(max_length=255)
    creditosAsignaturaBAFA = models.IntegerField()
    fechaInicioAsignaturaBAFA = models.DateField()
    fechaFinalizacionAsignaturaBAFA = models.DateField()
    profesorAsignaturaBAFA = models.CharField(max_length=255)
    silaboAsignaturaBAFA = models.FileField(upload_to='asignaturas', null=True, blank=True)
    metodoEvaluacionBAFA = models.CharField(max_length=255)
    recursosDidacticosBAFA = models.TextField()

    curso = models.ForeignKey(CursoBAFA, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        fila = "{0}: {1} {2} {3} {4} {5} {6} {7}"
        return fila.format(
            self.idAsignaturaBAFA, self.nombreAsignaturaBAFA, self.creditosAsignaturaBAFA,
            self.fechaInicioAsignaturaBAFA, self.fechaFinalizacionAsignaturaBAFA,
            self.profesorAsignaturaBAFA, self.silaboAsignaturaBAFA,
            self.metodoEvaluacionBAFA, self.recursosDidacticosBAFA
        )
