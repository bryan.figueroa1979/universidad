from django.contrib import admin
from .models import CarreraBAFA
from .models import CursoBAFA
from .models import AsignaturaBAFA
# Register your models here. hacer un crud con una sola linea
admin.site.register(CarreraBAFA)
admin.site.register(CursoBAFA)
admin.site.register(AsignaturaBAFA)
